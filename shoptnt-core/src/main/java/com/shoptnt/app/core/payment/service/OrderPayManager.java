/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.payment.service;

import com.shoptnt.app.core.payment.model.dto.PayParam;

import java.util.Map;

/**
 * @author fk
 * @version v2.0
 * @Description: 订单支付
 * @date 2018/4/1617:09
 * @since v7.0.0
 */
public interface OrderPayManager {

    /**
     * 支付
     *
     * @param param
     * @return
     */
    Map pay(PayParam param);


}
