/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.system.impl;

import com.shoptnt.app.core.base.SettingGroup;
import com.shoptnt.app.core.base.service.SettingManager;
import com.shoptnt.app.core.client.system.SettingClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v1.0
 * @Description: 系统设置
 * @date 2018/7/30 10:49
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class SettingClientDefaultImpl implements SettingClient {

    @Autowired
    private SettingManager settingManager;

    @Override
    public void save(SettingGroup group, Object settings) {

        this.settingManager.save(group, settings);

    }

    @Override
    public String get(SettingGroup group) {
        return this.settingManager.get(group);
    }
}
