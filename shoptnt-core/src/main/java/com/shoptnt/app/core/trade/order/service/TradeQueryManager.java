/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.trade.order.service;

import com.shoptnt.app.core.trade.order.model.dos.TradeDO;

/**
 * 交易查询接口
 *
 * @author Snow create in 2018/5/21
 * @version v2.0
 * @since v7.0.0
 */
public interface TradeQueryManager {

    /**
     * 根据交易单号查询交易对象
     *
     * @param tradeSn
     * @return
     */
    TradeDO getModel(String tradeSn);

}
