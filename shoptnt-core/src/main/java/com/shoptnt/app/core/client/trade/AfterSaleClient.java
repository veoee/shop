/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.trade;

import com.shoptnt.app.core.aftersale.model.dos.RefundGoodsDO;

import java.util.List;

/**
 * @author fk
 * @version v2.0
 * @Description: 售后client
 * @date 2018/8/13 16:01
 * @since v7.0.0
 */
public interface AfterSaleClient {

    /**
     * 查询退款单状态
     */
    void queryRefundStatus();

    /**
     * 获取退货单的商品列表
     * @param sn 退款单号
     * @return 退货商品列表
     */
    List<RefundGoodsDO> getRefundGoods(String sn);

}
