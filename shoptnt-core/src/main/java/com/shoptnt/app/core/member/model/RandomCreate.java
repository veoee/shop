/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.member.model;

import com.shoptnt.app.core.base.SettingGroup;
import com.shoptnt.app.core.base.service.SettingManager;
import com.shoptnt.app.core.system.model.vo.SiteSetting;
import com.shoptnt.app.framework.context.ApplicationContextHolder;
import com.shoptnt.app.framework.util.JsonUtil;

/**
 * 随机验证码生成
 *
 * @author zh
 * @version v7.0
 * @date 18/4/24 下午8:06
 * @since v7.0
 */

public class RandomCreate {

    public static String getRandomCode() {
        // 随机生成的动态码
        String dynamicCode = "" + (int) ((Math.random() * 9 + 1) * 100000);
        //如果是测试模式，验证码为1111
        SettingManager settingManager = (SettingManager) ApplicationContextHolder.getBean("settingManagerImpl");
        String siteSettingJson = settingManager.get(SettingGroup.SITE);

        SiteSetting setting = JsonUtil.jsonToObject(siteSettingJson,SiteSetting.class);
        if (setting == null || setting.getTestMode() == null || setting.getTestMode().equals(1)) {
            dynamicCode = "1111";
        }
        return dynamicCode;
    }

}
