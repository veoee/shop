/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.trade.impl;

import com.shoptnt.app.core.client.trade.PromotionGoodsClient;
import com.shoptnt.app.core.promotion.seckill.service.SeckillGoodsManager;
import com.shoptnt.app.core.promotion.tool.service.PromotionGoodsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * 促销商品客户端实现
 *
 * @author zh
 * @version v7.0
 * @date 19/3/28 上午11:30
 * @since v7.0
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class PromotionGoodsClientDefaultImpl implements PromotionGoodsClient {

    @Autowired
    private PromotionGoodsManager promotionGoodsManager;

    @Autowired
    private SeckillGoodsManager seckillGoodsManager;


    @Override
    public void delPromotionGoods(Integer goodsId) {
        //删除促销商品
        promotionGoodsManager.delete(goodsId);
        //删除限时抢购商品
        seckillGoodsManager.deleteSeckillGoods(goodsId);

    }
}
