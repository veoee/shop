/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.member.constraint.validator;

import com.shoptnt.app.core.member.constraint.annotation.GradeType;
import com.shoptnt.app.core.member.model.enums.CommentGrade;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author fk
 * @version v2.0
 * @Description: GradeType 验证
 * @date 2018/4/3 11:44
 * @since v7.0.0
 */
public class GradeTypeValidator implements ConstraintValidator<GradeType, String> {

    @Override
    public void initialize(GradeType status) {

    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        try {
            CommentGrade.valueOf(value);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }

    }

}

