/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.statistics.impl;

import com.shoptnt.app.core.client.statistics.OrderDataClient;
import com.shoptnt.app.core.statistics.service.OrderDataManager;
import com.shoptnt.app.core.trade.order.model.dos.OrderDO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * OrderDataClientDefaultImpl
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-14 下午2:41
 */

@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class OrderDataClientDefaultImpl implements OrderDataClient {

    @Autowired
    private OrderDataManager orderDataManager;
    /**
     * 订单新增
     *
     * @param order
     */
    @Override
    public void put(OrderDO order) {
        orderDataManager.put(order);
    }

    /**
     * 订单修改
     *
     * @param order
     */
    @Override
    public void change(OrderDO order) {
        orderDataManager.change(order);
    }
}
