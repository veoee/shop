/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.system.impl;

import com.shoptnt.app.core.client.system.NoticeLogClient;
import com.shoptnt.app.core.system.model.dos.NoticeLogDO;
import com.shoptnt.app.core.system.service.NoticeLogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v2.0
 * @Description: 店铺消息
 * @date 2018/8/14 10:22
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class NoticeLogClientDefaultImpl implements NoticeLogClient {


    @Autowired
    private NoticeLogManager noticeLogManager;

    @Override
    public NoticeLogDO add(NoticeLogDO shopNoticeLog) {
        return noticeLogManager.add(shopNoticeLog);
    }
}
