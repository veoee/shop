/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.client.statistics;

/**
 * 统计数据填充
 * @author liushuai
 * @version v1.0
 * @since v7.0
 * 2018/8/14 下午1:03
 * @Description:
 *
 */
public interface SyncopateTableClient {


    /**
     * 每日数据填充
     */
    void everyDay();
}
