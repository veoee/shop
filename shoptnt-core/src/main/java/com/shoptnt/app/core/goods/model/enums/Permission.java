/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.core.goods.model.enums;


/**
 * 权限枚举
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月20日 上午10:39:41
 */
public enum Permission {
    /**
     * 买家权限
     */
    BUYER,

    /**
     * 管理员权限
     */
    ADMIN,

    /**
     * 不验证权限
     */
    CLIENT

}
