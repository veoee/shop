/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.consumer.core.event;

/**
 * 页面生成事件
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018年3月23日 上午9:52:43
 */
public interface PageCreateEvent {

	/**
	 * 生成
	 * @param choosePages
	 */
	void createPage(String[] choosePages);
}
