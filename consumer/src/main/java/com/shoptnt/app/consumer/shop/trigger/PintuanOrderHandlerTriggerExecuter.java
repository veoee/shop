/*
 * 三河市峰颖软件技术有限公司 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
*/
package com.shoptnt.app.consumer.shop.trigger;

import com.shoptnt.app.core.promotion.pintuan.service.PintuanOrderManager;
import com.shoptnt.app.framework.trigger.Interface.TimeTriggerExecuter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 评团订单延时处理
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2019-03-07 下午5:38
 */
@Component("pintuanOrderHandlerTriggerExecuter")
public class PintuanOrderHandlerTriggerExecuter implements TimeTriggerExecuter {


    @Autowired
    private PintuanOrderManager pintuanOrderManager;


    /**
     * 执行任务
     *
     * @param object 任务参数
     */
    @Override
    public void execute(Object object) {

        //pintuanOrderManager.handle((int) object);


    }
}
