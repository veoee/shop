${Ansi.CYAN}   _____ _              _______ _   _ _______
${Ansi.CYAN}  / ____| |            |__   __| \ | |__   __|
${Ansi.CYAN} | (___ | |__   ___  _ __ | |  |  \| |  | |
${Ansi.CYAN}  \___ \| '_ \ / _ \| '_ \| |  | . ` |  | |
${Ansi.CYAN}  ____) | | | | (_) | |_) | |  | |\  |  | |
${Ansi.CYAN} |_____/|_| |_|\___/| .__/|_|  |_| \_|  |_|
${Ansi.CYAN}                    | |
${Ansi.CYAN}                    |_|
${Ansi.CYAN}
${AnsiColor.WHITE} Running Spring Boot ${spring-boot.version}